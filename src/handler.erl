-module(handler).
-export([start/3]).

start(Client, Validator, Store) ->
  spawn_link(fun() -> init(Client, Validator, Store) end).

init(Client, Validator, Store) ->
  handler(Client, Validator, Store, [], []).

handler(Client, Validator, Store, Reads, Writes) ->
  receive
    {read, Ref, N} ->
      case lists:keyfind(N, 1, Writes) of  %% TO DO: COMPLETE
        {N, _, Value} ->
%% TO DO: ADD SOME CODE
          Client ! {value, Ref, Value},
          handler(Client, Validator, Store, Reads, Writes);
        false ->
%% TO DO: ADD SOME CODE
%% TO DO: ADD SOME CODE
          %%% NOSURE: HOW TO FIND THE N IN THE STORE
          store:lookup(N, Store) ! {read, Ref, self()},
          handler(Client, Validator, Store, Reads, Writes)
      end;

    {Ref, Entry, Value, Time} ->
%% TO DO: ADD SOME CODE HERE AND COMPLETE NEXT LINE
      Client ! {value, Ref, Value},
      handler(Client, Validator, Store, [{Entry, Time} | Reads], Writes);

    {write, N, Value} ->
%% TO DO: ADD SOME CODE HERE AND COMPLETE NEXT LINE
      Entry = store:lookup(N, Store),
      Added = lists:keystore(N, 1, Writes, {N, Entry, Value}),
      handler(Client, Validator, Store, Reads, Added);
    {commit, Ref} ->
%% TO DO: ADD SOME CODE
      Validator ! {Ref, Reads, Writes, Client};
    abort ->
      ok
  end.